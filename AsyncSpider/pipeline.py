# -*- coding: utf-8 -*-

"""
Datetime: 2019/6/14
Author: Zhang Yafei
Description: 
"""
import os
import pandas as pd


class CsvPipeline(object):
    """ csv数据存储管道 """
    def __init__(self, file_path):
        self.file_path = file_path
        self.data = []

    async def add_item(self, item):
        self.data.append(item)

    async def item_completed(self):
        df = pd.DataFrame(data=self.data)
        if os.path.exists(self.file_path):
            df.to_csv(self.file_path, index=False, mode='a', header=False, encoding='utf_8_sig')
        else:
            df.to_csv(self.file_path, index=False, encoding='utf_8_sig')



