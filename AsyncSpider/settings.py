# -*- coding: utf-8 -*-

"""
Datetime: 2019/6/11
Author: Zhang Yafei
Description: 
"""
import os

DIR_PATH = os.path.abspath(os.path.dirname(__file__))

# 爬虫项目模块类路径
Spider_Name = 'spiders.xiaohua1.XiaohuaSpider'
# Spider_Name = 'spiders.xiaohua.XiaohuaSpider'

# PIPELINE
PIPELINE = 'pipeline.CsvPipeline'
TO_FILE = 'xiaohua1.csv'

# 全局headers
headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36'}

# 若要保存图片，设置文件夹
IMAGE_DIR = 'images'

if not os.path.exists(IMAGE_DIR):
    os.mkdir(IMAGE_DIR)
